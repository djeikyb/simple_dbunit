package:
	perl -pi -w -e 's:^//package :package :' *java

unpackage:
	perl -pi -w -e 's:^package ://package :' *java

tests: TestCase.java
	javac -Xlint:unchecked TestCase.java && \
	java org.junit.runner.JUnitCore TestCase

clean:
	rm *.class

