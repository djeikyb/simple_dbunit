//package simple_dbunit;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mysql.jdbc.Driver;

import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.Assertion;


import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class TestCase
{

  private IDatabaseTester database_tester;


  public IDataSet getDataSet() throws FileNotFoundException, DataSetException
  {
    return new FlatXmlDataSetBuilder().build(
        /*
        new FileInputStream("src/simple_dbunit/expected_dataset.xml"));
        new FileInputStream("dataset.xml"));
         */
        new FileInputStream("dataset.xml"));
  }


  @Before
  public void setUp() throws Exception
  {
    database_tester = new JdbcDatabaseTester("com.mysql.jdbc.Driver",
                                            "jdbc:mysql://localhost/cal",
                                            "cal",
                                            "cal");
    database_tester.setDataSet(getDataSet());
    database_tester.onSetup();
  }

  @Test
  public void testDbNoChanges() throws Exception
  {
    // expected
    IDataSet expected_data_set = getDataSet();

    // actual
    IDatabaseConnection connection = database_tester.getConnection();
    IDataSet actual_data_set = connection.createDataSet();

    // test
    Assertion.assertEquals(expected_data_set, actual_data_set);
  }

  @Test
  public void testTableNoChanges() throws Exception
  {
    // expected
    ITable expected_table = getDataSet().getTable("test");

    // actual
    IDatabaseConnection connection = database_tester.getConnection();
    IDataSet actual_data_set = connection.createDataSet();
    ITable actual_table = actual_data_set.getTable("test");

    // test
    Assertion.assertEquals(expected_table, actual_table);
  }

  @Test
  public void testTableNoChanges1() throws Exception
  {
    // expected
    ITable expected_table = getDataSet().getTable("test");

    // actual
    IDatabaseConnection connection = database_tester.getConnection();
    IDataSet actual_data_set = connection.createDataSet();
    ITable actual_table = actual_data_set.getTable("test");

    // test
    Assertion.assertEquals(expected_table, actual_table);
  }

}
